//minteri2 jmantic2
console.log('page load - entered main.js for js');

//Creating global variables
var first = true;
var first_2 = true;
var yearsAr = [];
var recAr = [];
var srsAr = [];
var ptsForAr = [];
var ptsAgtAr = [];

//Creating onmouseups
var yearArray = false;
var addYearButton = document.getElementById('add-year');
addYearButton.onmouseup = addYear;

var submitButton = document.getElementById('send-button');
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = resetAll;

var deleteButton = document.getElementById('delete-button');
deleteButton.onmouseup = getDeleteInfo;

var putButton = document.getElementById('put-button');
putButton.onmouseup = getPutInfo;

var postButton = document.getElementById('post-button');
postButton.onmouseup = getPostInfo;

var resetButton = document.getElementById('reset-button');
resetButton.onmouseup = getResetInfo;

//Function to add year for comparison
function addYear(){
  var form = document.getElementById('add-years-text');
  input = document.createElement('input');
  input.setAttribute('type', 'text');
  input.setAttribute('id', 'year-text');
  input.setAttribute('class', 'form-control')
  form.appendChild(input);
  yearArray = true;

}

//Get the form information for GET requests
function getFormInfo(){
    console.log('entered getFormInfo!');

    var all;
    var message = null;

    /*
    Checking if it is first time, two booleans because onload function comes
    after everything and we need to check if it is the first time in the
    onload part of the request
    */
    if (!first_2 && first){
      first = false;
    }

    if (first_2) {
      first_2 = false;
    }

    //If years were provided
    if (yearArray){
      all = false;
      if (first){
        var years = document.querySelectorAll("[id='year-text']");
        p = document.getElementById("description-add-years");
        p.innerHTML = "Add more years for comparison";
        var i;
        for (i = 0; i < years.length; i++){
          yearsAr.push(years[i].value);
          console.log(years[i].value + " added for comparison");
        }
      }
    }
    else{
      all = true;
    }

    var form = document.getElementById('add-years-text');
    form.innerHTML = "";
    document.getElementById("add-year").disabled = true;
    p = document.getElementById("description-add-years");
    p.innerHTML = "";



    //If it is the first time we go through
    if (first){
      //Adding the Dropdown menu for sorting parameters
      var form = document.getElementById('select-stat-div');
      label = document.createElement('label');
      label.innerHTML = 'Select Stat to Sort Data By';
      select = document.createElement('select')
      select.setAttribute('class', 'form-control')
      select.setAttribute('id', 'select-stat');
      option = document.createElement('option');
      option.innerHTML = 'Year';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Record';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'SRS (Simple Rating System)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points For';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points Against';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Record (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'SRS (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points For (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points Against (ONLY)';
      select.appendChild(option);
      form.appendChild(label);
      form.appendChild(select);

      submitButton.innerHTML = "Sort";
    }

    //Getting parameters to make request
    var server_addr = "http://student04.cse.nd.edu";
    console.log('Selected Server: ' + server_addr);

    var port = "51067";
    console.log('Port Number: ' + port);

    var request_type = "GET";

    var request_stat = document.getElementById('select-stat').value;

    //for iteration and to check if the user wants to display only one stat
    var i;
    var single;
    var singleStat;
    var stat;
    if (request_stat == "Year"){
      yearsAr.sort();
      yearsAr.reverse();
    }
    else if(request_stat == "Record"){
      sort_stat('r', false);
      single = false;
    }
    else if(request_stat == "SRS (Simple Rating System)"){
      sort_stat('s', false);
      single = false;
    }
    else if(request_stat == "Points For"){
      sort_stat('pf', false);
      single = false;
    }
    else if(request_stat == "Points Against"){
      sort_stat('pa', false);
      single = false;
    }
    else if(request_stat == "Record (ONLY)"){
      sort_stat('r', true);
      single = true;
      singleStat = "Record";
      stat = "record";
    }
    else if(request_stat == "SRS (ONLY)"){
      sort_stat('s', true);
      single = true;
      singleStat = "SRS";
      stat = "srs";
    }
    else if(request_stat == "Points For (ONLY)"){
      sort_stat('pf', true);
      single = true;
      singleStat = "Points For";
      stat = "points_for";
    }
    else{
      sort_stat('pa', true);
      single = true;
      singleStat = "Points Against";
      stat = "points_against";
    }

    //If user wants to display all stats
    if (!single){
      createFullTable();
      if (!all){
        var key = null;
        //Making GET request to each of the years the user wants to compare
        for(i = 0; i < yearsAr.length; i++){
          key = yearsAr[i];
          makeRequestGet(server_addr, port, request_type, key, message);
          sleep(50);
        }
      }
      else{
        yearArray = true;
        //Making GET request to all years
        makeRequestGet(server_addr, port, request_type, key, message);
      }
    }

    //If user wants to display a specific stat only
    else{
      createPartialTable(singleStat);
      var key = null;
      for(i = 0; i < yearsAr.length; i++){
        key = yearsAr[i];
        makeRequestSingle(server_addr, port, request_type, key, stat, message);
        sleep(50);
      }
    }

} // end of get form info

function makeRequestGet(server_addr, port, request_type, key, message){
    var xhr = new XMLHttpRequest();

    var url = server_addr + ":" + port + "/stats/";
    if(key) {
        url += "year/" + key;
    }

    console.log("Making request to:" + url);
    xhr.open(request_type, url, true);

    if (key){
      xhr.onload = function(e) {
          updateText(xhr.responseText, key);
      }
    }
    else{
      xhr.onload = function(e) {
          updateTextAll(xhr.responseText);
      }
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(message)

}

function updateText(response_text, year){
  //If request was successful
  if(response_text[12] == 's'){
    var record = "";
    var srs = "";
    var points_for = "";
    var points_against = "";
    var i = 47;

    //Parsing all the values we need to update the table
    while (response_text[i] != "\""){
      record += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      srs += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      points_for += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != "}"){
      points_against += response_text[i];
      i++;
    }

    //If it is the first time, update the lists
    if (first){
      recAr.push(record);
      srsAr.push(srs);
      ptsForAr.push(points_for);
      ptsAgtAr.push(points_against);
    }

    //Updating table
    var table = document.getElementById("table");
    var tr = document.createElement("tr");
    table.appendChild(tr)
    var td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = record;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = srs;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_for;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_against;
    tr.appendChild(td);
  }
}


function updateTextAll(response_text){
  var i = 39;
  var year;
  var record;
  var srs;
  var points_for;
  var points_against;
  var table = document.getElementById("table");
  var tr;
  var td;
  //Parsing all values for all years
  while (response_text[i] != "]"){
    year = "";
    record = "";
    srs = "";
    points_for = "";
    points_against = "";
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ','){
      year += response_text[i]
      i++
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    i++;
    while (response_text[i] != "\""){
      record += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      srs += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      points_for += response_text[i];
      i++;
    }
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != "}"){
      points_against += response_text[i];
      i++;
    }
    i++;

    if (first){
      recAr.push(record);
      srsAr.push(srs);
      ptsForAr.push(points_for);
      ptsAgtAr.push(points_against);
    }

    //Updating table
    tr = document.createElement("tr");
    table.appendChild(tr)
    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = record;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = srs;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_for;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_against;
    tr.appendChild(td);
    }
}

function makeRequestSingle(server_addr, port, request_type, key, stat, message){
    var xhr = new XMLHttpRequest();

    var url = server_addr + ":" + port + "/stats/single/" + stat;
    if(key) {
        url += "/" + key;
    }

    console.log("Making request to:" + url);
    xhr.open(request_type, url, true);


    xhr.onload = function(e) {
        updateTextSingle(xhr.responseText, stat, key);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(message)
}

function updateTextSingle(response_text, stat, year){
  //If request was successful
  if(response_text[12] == 's'){
    var i = 35;
    var single = "";

    //Parsing stat
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    if (stat == "record"){
      i++;
      while (response_text[i] != "\""){
        single += response_text[i];
        i++;
      }
    }
    else{
      while(response_text[i] != "}"){
        single += response_text[i];
        i++
      }
    }

    //Updating table
    var table = document.getElementById("table");
    var tr = document.createElement("tr");
    table.appendChild(tr)
    var td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = single;
    tr.appendChild(td);

  }
}

//Function to sort data based on the stat the user wants to sort it by
function sort_stat(request){
  //resetting order of years
  yearsAr.sort();
  yearsAr.reverse();

  var i;
  var sorting = [];

  //If stat = record
  if (request == 'r'){
    var record;
    var w;
    var l;
    var t;
    var rec;
    for (i = 0; i < yearsAr.length; i++){
      record = recAr[i].split('-')
      w = parseFloat(record[0]);
      l = parseFloat(record[1]);
      t = parseFloat(record[2]);
      rec = (w + t/2)/(w + l + t);
      sorting.push({'year': yearsAr[i], 'rec': rec, 'w': w});
    }
    sorting.sort(function(a, b) {
      return ((a.rec < b.rec) ? -1 : ((a.rec == b.rec) ?  ((a.w < b.w) ? -1 : ((a.w == b.w) ? -1 : 1)): 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }

  //If stat = SRS
  else if (request == 's'){
    var srs;
    for (i = 0; i < yearsAr.length; i++){
      srs = parseFloat(srsAr[i]);
      sorting.push({'year': yearsAr[i], 'srs': srs});
    }
    sorting.sort(function(a, b) {
      return ((a.srs < b.srs) ? -1 : ((a.srs == b.srs) ? -1 : 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }

  //If stat = Points For
  else if (request == 'pf'){
    var pf;
    for (i = 0; i < yearsAr.length; i++){
      pf = parseInt(ptsForAr[i]);
      sorting.push({'year': yearsAr[i], 'pf': pf});
    }
    sorting.sort(function(a, b) {
      return ((a.pf < b.pf) ? -1 : ((a.pf == b.pf) ? -1 : 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }

  //If stat = Points Against
  else if (request == 'pa'){
    var pa;
    for (i = 0; i < yearsAr.length; i++){
      pa = parseInt(ptsAgtAr[i]);
      sorting.push({'year': yearsAr[i], 'pa': pa});
    }
    sorting.sort(function(a, b) {
      return ((a.pa < b.pa) ? -1 : ((a.pa == b.pa) ? -1 : 1));
      });
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }
}

//Function to create the full table
function createFullTable(){
  var table_div = document.getElementById("table-div");
  table_div.innerHTML = "";
  var table = document.createElement("table");
  table.setAttribute("style", "width: 100%");
  table.setAttribute("class", "table table-striped");
  var thead = document.createElement("thead");
  table.appendChild(thead);
  var tr = document.createElement("tr");
  thead.appendChild(tr);
  var th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "Year";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "Record";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "SRS";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "Points For";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "Points Against";
  tr.appendChild(th);

  tbody = document.createElement('tbody');
  tbody.setAttribute("id", "table")
  table.appendChild(tbody);
  table_div.appendChild(table);
}

//Function to create table to only display year + specific stat
function createPartialTable(stat){
  var table_div = document.getElementById("table-div");
  table_div.innerHTML = "";
  var table = document.createElement("table");
  table.setAttribute("style", "width: %");
  table.setAttribute("class", "table table-striped");
  var thead = document.createElement("thead");
  table.appendChild(thead);
  var tr = document.createElement("tr");
  thead.appendChild(tr);
  var th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = "Year";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "text-align : center");
  th.innerHTML = stat;
  tr.appendChild(th);

  tbody = document.createElement('tbody');
  tbody.setAttribute("id", "table")
  table.appendChild(tbody);
  table_div.appendChild(table);
}

//get the form info to delete
function getDeleteInfo(){
  console.log("entered getDeleteInfo");

  //Getting parameters to make request
  var server_addr = "http://student04.cse.nd.edu";
  console.log('Selected Server: ' + server_addr);

  var port = "51067";
  console.log('Port Number: ' + port);

  var request_type = "DELETE";

  var message = null;

  var key = document.getElementById("delete-key").value;
  document.getElementById("delete-key").value = "";

  if (key == ""){
    key = null;
  }

  makeRequestDelete(server_addr, port, request_type, key, message);

}

function makeRequestDelete(server_addr, port, request_type, key, message){
  var xhr = new XMLHttpRequest();

  var url = server_addr + ":" + port + "/stats/";
  if(key) {
      url += key;
  }

  console.log("Making request to:" + url);
  xhr.open(request_type, url, true);


  xhr.onload = function(e) {
      console.log(xhr.responseText);
      updateDeleteText(xhr.responseText, key);
  }

  xhr.onerror = function(e) {
      console.error(xhr.statusText);
  }

  xhr.send(message)
}

function updateDeleteText(response_text, year){
  p = document.getElementById("delete-message");

  if(response_text[12] == 's'){
    if (year){
      p.innerHTML = year + " was deleted from existence";
    }
    else{
      p.innerHTML = "All years were deleted from existence";
    }

  }
  else{
    p.innerHTML = "Year not found, nothing was deleted";
  }
}

function getPutInfo(){
  console.log("entered getPutInfo");

  //Getting parameters to make request
  var server_addr = "http://student04.cse.nd.edu";
  console.log('Selected Server: ' + server_addr);

  var port = "51067";
  console.log('Port Number: ' + port);

  var request_type = "PUT";

  var key = document.getElementById("put-key").value;
  document.getElementById("put-key").value = "";

  var w = document.getElementById("put-wins").value;
  document.getElementById("put-wins").value = "";

  var l = document.getElementById("put-losses").value;
  document.getElementById("put-losses").value = "";

  var t = document.getElementById("put-ties").value;
  document.getElementById("put-ties").value = "";

  var srs = document.getElementById("put-srs").value;
  document.getElementById("put-srs").value = "";

  var pf = document.getElementById("put-pf").value;
  document.getElementById("put-pf").value = "";

  var pa = document.getElementById("put-pa").value;
  document.getElementById("put-pa").value = "";

  var message;
  message = '{\"record\": \"' + w + '-' + l + '-' + t + '\", \"srs\": ' + srs + ', \"points_for\": ' + pf + ', \"points_against\": ' + pa + '}';

  makeRequestPut(server_addr, port, request_type, key, message);
}

function makeRequestPut(server_addr, port, request_type, key, message){
  var xhr = new XMLHttpRequest();

  var url = server_addr + ":" + port + "/stats/" + key;

  console.log("Making request to:" + url);
  xhr.open(request_type, url, true);


  xhr.onload = function(e) {
      console.log(xhr.responseText);
      updatePutText(xhr.responseText, key);
  }

  xhr.onerror = function(e) {
      console.error(xhr.statusText);
  }

  xhr.send(message)
}

function updatePutText(response_text, year){
  p = document.getElementById("put-message");
  if(response_text[12] == 's'){
    p.innerHTML = year + " successfully modified";
  }
  else{
    p.innerHTML = "Year not found, nothing was modified";
  }
}

function getPostInfo(){
  console.log("entered getPostInfo");

  //Getting parameters to make request
  var server_addr = "http://student04.cse.nd.edu";
  console.log('Selected Server: ' + server_addr);

  var port = "51067";
  console.log('Port Number: ' + port);

  var request_type = "POST";

  var key = null;



  var w = document.getElementById("post-wins").value;
  document.getElementById("post-wins").value = "";

  var l = document.getElementById("post-losses").value;
  document.getElementById("post-losses").value = "";

  var t = document.getElementById("post-ties").value;
  document.getElementById("post-ties").value = "";

  var srs = document.getElementById("post-srs").value;
  document.getElementById("post-srs").value = "";

  var pf = document.getElementById("post-pf").value;
  document.getElementById("post-pf").value = "";

  var pa = document.getElementById("post-pa").value;
  document.getElementById("post-pa").value = "";

  var message;
  message = '{\"record\": \"' + w + '-' + l + '-' + t + '\", \"srs\": ' + srs + ', \"points_for\": ' + pf + ', \"points_against\": ' + pa + '}';

  makeRequestPost(server_addr, port, request_type, key, message);
}

function makeRequestPost(server_addr, port, request_type, key, message){
  var xhr = new XMLHttpRequest();

  var url = server_addr + ":" + port + "/stats/";

  console.log("Making request to:" + url);
  xhr.open(request_type, url, true);


  xhr.onload = function(e) {
      console.log(xhr.responseText);
      updatePostText(xhr.responseText);
  }

  xhr.onerror = function(e) {
      console.error(xhr.statusText);
  }

  xhr.send(message)
}

function updatePostText(response_text){
  p = document.getElementById("post-message");
  if(response_text[12] == 's'){
    p.innerHTML = "Year successfully added";
  }
  else{
    p.innerHTML = "Error, nothing was added";
  }
}

function getResetInfo(){
  console.log("entered getResetInfo");

  //Getting parameters to make request
  var server_addr = "http://student04.cse.nd.edu";
  console.log('Selected Server: ' + server_addr);

  var port = "51067";
  console.log('Port Number: ' + port);

  var request_type = "PUT";

  var message = null;

  var key = document.getElementById("reset-key").value;
  document.getElementById("reset-key").value = "";

  if (key == ""){
    key = null;
  }

  makeRequestReset(server_addr, port, request_type, key, message);

}

function makeRequestReset(server_addr, port, request_type, key, message){
  var xhr = new XMLHttpRequest();

  var url = server_addr + ":" + port + "/reset/";
  if(key) {
      url += key;
  }

  console.log("Making request to:" + url);
  xhr.open(request_type, url, true);


  xhr.onload = function(e) {
      console.log(xhr.responseText);
      updateResetText(xhr.responseText, key);
  }

  xhr.onerror = function(e) {
      console.error(xhr.statusText);
  }

  xhr.send(message)
}

function updateResetText(response_text, year){
  p = document.getElementById("reset-message");

  if(response_text[12] == 's'){
    if (year){
      p.innerHTML = year + " was reset to original";
    }
    else{
      p.innerHTML = "All years were reset to original";
    }

  }
  else{
    p.innerHTML = "Year not found, nothing was reset";
  }
}

//Function to clear all the table and start again
function resetAll(){
  yearArray = false;
  first = true;
  first_2 = true;
  var resetSelect = document.getElementById('select-stat-div');
  resetSelect.innerHTML = "";
  var resetYears = document.getElementById('add-years-text');
  resetYears.innerHTML = "";
  var resetTable = document.getElementById("table-div");
  resetTable.innerHTML = "";
  var resetMessage = document.getElementById("description-add-years");
  resetMessage.innerHTML = "Add the years for which you wish to compare stats. If none are added, data for all years will be displayed. Also, no invalid years will be taken into account.";
  document.getElementById("add-year").disabled = false;
  submitButton.innerHTML = "Search";
  yearsAr = [];
  recAr = [];
  srsAr = [];
  ptsForAr = [];
  ptsAgtAr = [];

}

//Function to delay the flow of the requests to make sure they go in order
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
