import unittest
from stats_library import _stats_database

class TestStatsAPI(unittest.TestCase):

    sdb = _stats_database()
    sdb.load_stats('stats.dat')

    def reload_sdb(self):
        self.sdb = _stats_database()
        self.sdb.load_stats('stats.dat')

    def test_get_stats(self):
        self.assertEqual(len(self.sdb.get_stats1()), len(self.sdb.record))

    def test_get_stat(self):

        stats = self.sdb.get_stats(2020)
        self.assertEqual(stats[0], "7-0-0")
        self.assertEqual(stats[1], 15.58)
        self.assertEqual(stats[2], 256)
        self.assertEqual(stats[3], 102)

        stats = self.sdb.get_stats(2019)
        self.assertEqual(stats[2], 478)

    def test_get__single_stat(self):

        stats = self.sdb.get_single_stat(2020, 'record')
        self.assertEqual(stats, "7-0-0")

        stats = self.sdb.get_single_stat(2019, 'points_for')
        self.assertEqual(stats, 478)


    def test_set_stats(self):
        self.reload_sdb()

        self.sdb.set_stats(2020, list(("13-0-0", 30.0, 400, 100)))

        stats = self.sdb.get_stats(2020)
        self.assertEqual(stats[0], "13-0-0")
        self.assertEqual(stats[1], 30.0)
        self.assertEqual(stats[2], 400)
        self.assertEqual(stats[3], 100)


    def test_delete_stats(self):
        self.reload_sdb()

        self.sdb.delete_stats(2020)

        self.assertFalse(self.sdb.get_stats(2020))

if __name__ == "__main__":
    unittest.main()
