import unittest
import requests
import json


class TestStatsIndex(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51067' # replace with your assigned port id
    print("Testing for server: " + SITE_URL)
    STATS_URL = SITE_URL + '/stats/'
    YEAR_URL = STATS_URL + 'year/'
    SINGLE_URL = STATS_URL + 'single/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_stats_index_get(self):
        self.reset_data()
        r = requests.get(self.STATS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        teststat = {}
        stats = resp['stats']
        for stat in stats:
            if stat['year'] == 2019:
                teststat = stat

        self.assertEqual(teststat['record'], '11-2-0')
        self.assertEqual(teststat['srs'], 17.22)
        self.assertEqual(teststat['points_for'], 478)
        self.assertEqual(teststat['points_against'], 233)

    def test_stats_index_post(self):
        self.reset_data()

        s = {}
        s['record'] = '14-0'
        s['srs'] = 12.34
        s['points_for'] = 450
        s['points_against'] = 150
        r = requests.post(self.STATS_URL, data = json.dumps(s))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['year'], 2021)

        r = requests.get(self.YEAR_URL + str(resp['year']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['record'], s['record'])
        self.assertEqual(resp['srs'], s['srs'])
        self.assertEqual(resp['points_for'], s['points_for'])
        self.assertEqual(resp['points_against'], s['points_against'])

    def test_stats_index_delete(self):
        self.reset_data()

        m = {}
        r = requests.delete(self.STATS_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')


        r = requests.get(self.STATS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        stats = resp['stats']
        self.assertFalse(stats)


if __name__ == "__main__":
    unittest.main()
