import cherrypy
import re, json
from stats_library import _stats_database

class StatsController(object):

        def __init__(self, sdb=None):
                if sdb is None:
                        self.sdb = _stats_database()
                else:
                        self.sdb = sdb

                self.sdb.load_stats('stats.dat')

        def GET_KEY(self, year:int) -> str:
            '''when GET request for /stats/year comes in, then we respond with json string'''
            output = {'result':'success'}
            year = int(year)


            try:
                stats = self.sdb.get_stats(year)
                if stats is not None:
                    output['year'] = year
                    output['record'] = stats[0]
                    output['srs'] = stats[1]
                    output['points_for'] = stats[2]
                    output['points_against'] = stats[3]
                else:
                    output['result'] = 'error'
                    output['message'] = 'stats not found'
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)

        def PUT_KEY(self, year):
            '''when PUT request for /stats/year comes in, then we change that stats in the sdb'''
            output = {'result':'success'}
            year = int(year)

            data = json.loads(cherrypy.request.body.read().decode('utf-8'))

            stats = list()
            stats.append(data['record'])
            stats.append(data['srs'])
            stats.append(data['points_for'])
            stats.append(data['points_against'])

            self.sdb.set_stats(year, stats)

            return json.dumps(output)

        def DELETE_KEY(self, year):
            '''when DELETE for /stats/year comes in, we remove just that stats from sdb'''
            output = {'result':'success'}
            year = int(year)

            try:
                self.sdb.delete_stats(year)
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)


        def GET_INDEX(self):
            '''when GET request for /stats/ comes in, we respond with all the stats information in a json str'''
            output = {'result':'success'}
            output['stats'] = []

            try:
                for year in self.sdb.get_stats1().keys():
                    stats = self.sdb.get_stats(year)
                    dstats = {'year':year, 'record':stats[0],
                    'srs':stats[1], 'points_for':stats[2], 'points_against':stats[3]}
                    output['stats'].append(dstats)
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)

        def POST_INDEX(self):
            '''when POST for /stats/ comes in, we take title and genres from body of request, and respond with the new year and more'''
            output = {'result':'success'}

            data = json.loads(cherrypy.request.body.read().decode('utf-8'))

            stats = list()
            stats.append(data['record'])
            stats.append(data['srs'])
            stats.append(data['points_for'])
            stats.append(data['points_against'])


            year = max(i for i in self.sdb.get_stats1().keys())

            year += 1

            self.sdb.set_stats(year, stats)
            output['year'] = year

            return json.dumps(output)

        def DELETE_INDEX(self):
            '''when DELETE for /stats/ comes in, we remove each existing stats from sdb object'''
            output = {'result':'success'}

            try:
                self.sdb.record.clear()
                self.sdb.srs.clear()
                self.sdb.points_for.clear()
                self.sdb.points_against.clear()
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)


        def GET_SINGLE_STAT(self, year, stat):
            output = {'result':'success'}
            year = int(year)


            try:
                stats = self.sdb.get_stats(year)
                if stats is not None:
                    output['year'] = year
                    if stat == "record":
                        output['record'] = stats[0]
                    elif stat == "srs":
                        output['srs'] = stats[1]
                    elif stat == "points_for":
                        output['points_for'] = stats[2]
                    else:
                        output['points_against'] = stats[3]
                else:
                    output['result'] = 'error'
                    output['message'] = 'stats not found'
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)
