import cherrypy
from statsController import StatsController
from resetController import ResetController
from stats_library import _stats_database

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"


def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()


    sdb = _stats_database()

    statsController     = StatsController(sdb=sdb)
    resetController     = ResetController(sdb=sdb)

    dispatcher.connect('stats_get', '/stats/year/:year', controller=statsController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('stats_put', '/stats/:year', controller=statsController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('stats_delete', '/stats/:year', controller=statsController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('stats_index_get', '/stats/', controller=statsController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('stats_index_post', '/stats/', controller=statsController, action = 'POST_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('stats_index_delete', '/stats/', controller=statsController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('stat_single_get', '/stats/single/:stat/:year', controller=statsController, action = 'GET_SINGLE_STAT', conditions=dict(method=['GET']))


    dispatcher.connect('reset_put', '/reset/:year', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    #CORS
    dispatcher.connect('stats_key_options', '/stats/year/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_year_options', '/stats/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_options', '/stats/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_single_key_options', '/stats/single/:stat/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_single_options', '/stats/single/:stat', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_key_options', '/reset/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))


    conf = {
	'global': {
        'server.thread_pool': 5, # optional argument
	    'server.socket_host': 'student04.cse.nd.edu', #
	    'server.socket_port': 51067, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
        'tools.CORS.on': True,
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
