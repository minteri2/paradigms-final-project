
class _stats_database:

       def __init__(self):
        self.record = dict()
        self.srs = dict()
        self.points_for = dict()
        self.points_against = dict()

       def load_stats(self, stats_file):
        f = open(stats_file)
        for line in f:
                line = line.rstrip()
                components = line.split("::")
                year = int(components[0])
                record = str(components[1]) + '-' + str(components[2]) + '-' + str(components[3])
                srs = float(components[4])
                points_for = int(components[5])
                points_against = int(components[6])
                self.record[year] = record
                self.srs[year] = srs
                self.points_for[year] = points_for
                self.points_against[year] = points_against
        f.close()

       def get_stats1(self):
        all = {}

        for year in self.record.keys():
                record = self.record[year]
                srs = self.srs[year]
                points_for = self.points_for[year]
                points_against = self.points_against[year]
                year_stats = list((record, srs, points_for, points_against))
                all[year] = year_stats
        return all

       def get_stats(self, year):
        try:
                record = self.record[year]
                srs = self.srs[year]
                points_for = self.points_for[year]
                points_against = self.points_against[year]
                year_stats = list((record, srs, points_for, points_against))
        except Exception as ex:
                year_stats = None
        return year_stats

       def get_single_stat(self, year, stat):
        try:
                if stat == "record":
                        out = self.record[year]
                elif stat == "srs":
                        out = self.srs[year]
                elif stat == "points_for":
                        out = self.points_for[year]
                else:
                        out = self.points_against[year]
        except Exception as ex:
                out = None
        return out


       def set_stats(self, year, year_stats):
        self.record[year] = year_stats[0]
        self.srs[year] = year_stats[1]
        self.points_for[year] = year_stats[2]
        self.points_against[year] = year_stats[3]


       def delete_stats(self, year):
        del(self.record[year])
        del(self.srs[year])
        del(self.points_for[year])
        del(self.points_against[year])


if __name__ == "__main__":
       sdb = _stats_database()

       #### MOVIES ########
       sdb.load_stats('stats.dat')

       for i in rangesdb.get_stats1():
           for year in range(1899,1899 + len(self.sdb.get_stats1())):
               stats = self.sdb.get_stats(year)
               print(stats)

       movie[0] = '12-4-0'
       sdb.set_stats(2000, movie)

       movie = sdb.get_stats(2000)
       print(movie)
       print(sdb.get_single_stat(2019,"srs"))
       years = [2016, 2014, 2018, 2015, 2017, 2019, 2020]
       years = sdb.sort_years(years, "points_against")
       print(years)
       # for year in years:
       #     print(sdb.get_stats(year))
       ####################

       #### RATINGS #######
       # hrm_mid = sdb.get_highest_rated_movie()
       # hrm_rating = sdb.get_rating(hrm_mid)
       # hrm = sdb.get_stats(hrm_mid)
       # hrm_name = hrm[0]
       # print(hrm_mid, hrm_name, hrm_rating)
       ####################
