import unittest
import requests
import json
from stats_library import _stats_database

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51067' # replace with your port id
    print("Testing for server: " + SITE_URL)
    STATS_URL = SITE_URL + '/stats/'
    YEAR_URL = STATS_URL + 'year/'
    RESET_URL = SITE_URL + '/reset/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        # Change a Movie
        m = {}
        m['record'] = '3-8-1'
        m['srs'] = -10.5
        m['points_for'] = 250
        m['points_against'] = 350
        r = requests.post(self.STATS_URL, data = json.dumps(m))

        # Reset movie
        r = requests.put(self.RESET_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Check reset
        r = requests.get(self.STATS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Create local db to check against
        sdb = _stats_database()
        sdb.load_stats('stats.dat')

        output = {'result': 'success'}
        output['stats'] = []
        for year in sdb.get_stats1().keys():
            stats = sdb.get_stats(year)
            dstats = {'year':year, 'record':stats[0],
            'srs':stats[1], 'points_for':stats[2], 'points_against':stats[3]}
            output['stats'].append(dstats)

        # Local db to check reset
        self.assertEqual(output, resp)


    def test_put_reset_key(self):
        # Change value of Movie
        year = 2019
        m = {}
        m['record'] = '3-8-1'
        m['srs'] = -10.5
        m['points_for'] = 250
        m['points_against'] = 350
        r = requests.put(self.STATS_URL + str(year), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        # Reset to Original
        r = requests.put(self.RESET_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Check for succesfull reset
        r = requests.get(self.YEAR_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['record'], '11-2-0')
        self.assertEqual(resp['srs'], 17.22)
        self.assertEqual(resp['points_for'], 478)
        self.assertEqual(resp['points_against'], 233)



if __name__ == "__main__":
    unittest.main()
