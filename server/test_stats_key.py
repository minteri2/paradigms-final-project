import unittest
import requests
import json

class TestStats(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51067' # replace with your port number and
    print("testing for server: " + SITE_URL)
    STATS_URL = SITE_URL + '/stats/'
    YEAR_URL = STATS_URL + 'year/'
    SINGLE_URL = STATS_URL + 'single/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_stats_get_key(self):
        self.reset_data()
        year = 2019
        r = requests.get(self.YEAR_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['record'], '11-2-0')
        self.assertEqual(resp['srs'], 17.22)
        self.assertEqual(resp['points_for'], 478)
        self.assertEqual(resp['points_against'], 233)

    def test_movies_put_key(self):
        self.reset_data()
        year = 2019

        r = requests.get(self.YEAR_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['record'], '11-2-0')
        self.assertEqual(resp['srs'], 17.22)
        self.assertEqual(resp['points_for'], 478)
        self.assertEqual(resp['points_against'], 233)

        m = {}
        m['record'] = '14-0-0'
        m['srs'] = 25
        m['points_for'] = 450
        m['points_against'] = 150
        r = requests.put(self.STATS_URL + str(year), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.YEAR_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['record'], m['record'])
        self.assertEqual(resp['srs'], m['srs'])
        self.assertEqual(resp['points_for'], m['points_for'])
        self.assertEqual(resp['points_against'], m['points_against'])

    def test_movies_delete_key(self):
        self.reset_data()
        year = 2019

        m = {}
        r = requests.delete(self.STATS_URL + str(year), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.YEAR_URL + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

    def test_stats_single_key(self):
        self.reset_data()
        year = 2019
        stat = 'record/'
        r = requests.get(self.SINGLE_URL + stat + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['record'], '11-2-0')

        year = 1980
        stat = 'points_for/'
        r = requests.get(self.SINGLE_URL + stat + str(year))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['points_for'], 248)

if __name__ == "__main__":
    unittest.main()
